FROM alpine

RUN apk add --no-cache openssh-server openssh-client bash
RUN echo PasswordAuthentication no >> /etc/ssh/sshd_config \
    && echo PermitRootLogin without-password >> /etc/ssh/sshd_config

COPY entry.sh /entry.sh

EXPOSE 22

ENTRYPOINT ["/entry.sh"]

CMD ["/usr/sbin/sshd", "-D", "-E", "/var/log/sshd", "-f", "/etc/ssh/sshd_config"]
