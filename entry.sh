#!/bin/bash

chown root.root /root/.ssh/authorized_keys 
chmod 600 /root/.ssh/authorized_keys 
passwd -u root 

echo 'root:'$(cat /dev/urandom | env LC_CTYPE=C tr -dc a-zA-Z0-9 | head -c 64; echo) | chpasswd

/usr/sbin/sshd -D -E /var/log/sshd -f /etc/ssh/sshd_config
