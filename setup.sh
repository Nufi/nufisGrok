#!/bin/bash

[ "$#" -eq 1 ] || die "usage: setup.sh username@server.ch"

ssh-copy-id $1

scp nginx_app.conf $1:.
scp docker-compose.yml $1:.
scp compose.sh $1:.

ssh $1 -C ./compose.sh


echo start your server listening on localhost port 8888
sleep 10
ssh -N -R 0.0.0.0:3333:localhost:8888 root@$(echo $1 | rev | cut -d@ -f1 | rev) -p 2222


